# dont-return-this-number

## Problem Definition

We have array of integers called nums and an integer called x, write a function to return
all numbers (in a form of array of integers) that when divided by any of integers in nums
doesn't return x
for example : nums = [1,2,3,4], x = 4, your output should be : [1,2,3], because only 4
divided by 1 is 4 (x)

## How To Run

```
This project uses go test.
```

To run the tests run the command go test from within this directory.

```bash
go build
go test
```