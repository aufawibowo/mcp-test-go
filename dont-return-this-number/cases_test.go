package dontreturnthisnumber

var TestCases = []struct {
	description string
	nums        []int
	x           int
	expected    []int
}{
	{
		description: "default case",
		nums:        []int{1, 2, 3, 4},
		x:           4,
		expected:    []int{1, 2, 3},
	},
}
