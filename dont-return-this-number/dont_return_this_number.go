package dontreturnthisnumber

import "fmt"

// Solution function
func Solution(nums []int, x int) []int {
	// iterate through all int in nums
	for i, val := range nums {
		if val%x == 0 {
			nums = remove(nums, i)
			fmt.Println(i)
		}
	}
	return nums
}

func remove(slice []int, i int) []int {
	if len(slice) == i+1 {
		return append(slice[0:i])
	}
	return append(slice[:i], slice[i+1:]...)
}
