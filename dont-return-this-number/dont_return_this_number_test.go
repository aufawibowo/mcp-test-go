package dontreturnthisnumber

import (
	"testing"
)

func TestSolution(t *testing.T) {
	for _, tc := range TestCases {
		if actual := Solution(tc.nums, tc.x); !equal(tc.expected, actual) {
			t.Fatalf("FAIL: %s\nExpected: %#v\nActual: %#v", tc.description, tc.expected, actual)
		} else {
			t.Logf("PASS: %s\nExpected: %#v\nActual: %#v", tc.description, tc.expected, actual)
		}
	}
}

//func BenchmarkSublist(b *testing.B) {
//	for i := 0; i < b.N; i++ {
//		for _, tc := range TestCases {
//			Solution(tc.nums, tc.x)
//		}
//	}
//}

// Equal tells whether a and b contain the same elements.
// A nil argument is equivalent to an empty slice.
func equal(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}
