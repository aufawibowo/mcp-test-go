# dont-return-zero

## Problem Definition

We have array of integers called nums, write a function to return all numbers (in a form
of array of integers) that when subtracted by any of integers in nums doesn't return
number that is < 0
for example : nums = [3,1,4,2], your output should be : [4], because when 4 is subtracted
by 3 or 1 or 4 or 2 doesn't return number that is < 0

## How To Run

```
This project uses go test.
```

To run the tests run the command go test from within this directory.

```bash
go build
go test
```