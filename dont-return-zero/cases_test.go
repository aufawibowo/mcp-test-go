package dontreturnzero

var TestCases = []struct {
	description string
	nums        []int
	expected    int
}{
	{
		description: "default case",
		nums:        []int{1, 2, 3, 4},
		expected:    4,
	},
	{
		description: "more case",
		nums:        []int{-1, 0, 1, 2, 3, 4},
		expected:    4,
	},
}
