package dontreturnzero

// Solution function
func Solution(nums []int) int {
	var max int
	for _, value := range nums {
		if value > max {
			max = value
		}
	}
	return max
}
