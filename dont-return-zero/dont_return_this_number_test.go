package dontreturnzero

import (
	"testing"
)

func TestSolution(t *testing.T) {
	for _, tc := range TestCases {
		if actual := Solution(tc.nums); tc.expected != actual {
			t.Fatalf("FAIL: %s\nExpected: %#v\nActual: %#v", tc.description, tc.expected, actual)
		} else {
			t.Logf("PASS: %s\nExpected: %#v\nActual: %#v", tc.description, tc.expected, actual)
		}
	}
}
