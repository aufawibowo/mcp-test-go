# return-this-string

## Problem Definition

We have a string called word and an integer called x, write a function to return an array
of strings containing all strings that has length x.
for example : word = "souvenir loud four lost", x = 4, your output should be ["loud", "four",
"lost"] because those strings has length of 4

## How To Run

```
This project uses go test.
```

To run the tests run the command go test from within this directory.

```bash
go build
go test
```