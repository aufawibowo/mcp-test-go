package returnthisstring

var TestCases = []struct {
	description string
	word        string
	x           int
	expected    []string
}{
	{
		description: "default case",
		word:        "souvenir loud four lost",
		x:           4,
		expected: []string{
			"loud",
			"four",
			"lost",
		},
	},
	{
		description: "more case",
		word:        "makan bakso pake es jeruk",
		x:           5,
		expected: []string{
			"makan",
			"bakso",
			"jeruk",
		},
	},
}
