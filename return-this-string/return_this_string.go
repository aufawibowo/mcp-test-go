package returnthisstring

import (
	"strings"
)

func Solution(word string, length int) []string {
	var result []string

	words := strings.Fields(word)

	for _, word := range words {
		if len(word) == length {
			result = append(result, word)
		}
	}

	return result
}
