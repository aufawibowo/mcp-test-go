package returnthisstring

import "testing"

func TestSolution(t *testing.T) {
	for _, tc := range TestCases {
		if actual := Solution(tc.word, tc.x); dontHaveSameLength(tc.expected, actual) {
			t.Fatalf("FAIL: %s\nExpected: %#v\nActual: %#v", tc.description, tc.expected, actual)
		} else {
			t.Logf("PASS: %s\nExpected: %#v\nActual: %#v", tc.description, tc.expected, actual)
		}
	}
}

func dontHaveSameLength(expected, actual []string) bool {
	for i := range expected {
		if expected[i] != actual[i] {
			return true
		}
	}
	return false
}
